
% The project is hosted at Google code: http://code.google.com/p/pgf-umlsd/ 
%\documentclass{article}
\documentclass[letter,10pt,oneside]{book}

\usepackage{tikz}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{pgf-umlsd}
\usepgflibrary{arrows} % for pgf-umlsd
\RequirePackage{titlesec}
\usepackage{tcolorbox}
\usepackage[toc]{glossaries}
   
   
\include{defs}
\makeglossaries

%---------

\begin{document}
\AddToShipoutPicture*{\BackgroundPicTitle}
\begin{titlepage}
    \titlepagedecoration{%
        \titlefont WE-PN System \\Design\par
        \epigraph{System Design,\\ Infrastructure Overview,\\ Security Challenges}%
        {version 4 - \textit{2018.6}}
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%% CHANGES HERE %%%%%%%%%%%%%%%%
        %\tikz[remember picture,overlay]{%
        %    \node (X) {};
        %    \filldraw[opacity=0.5,draw=titlepagecolor,fill=titlepagecolor] (tp1 |- X) -- (X -| tp2) -- (tp2) -- (tp1) -- cycle;
        %}
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    }
    \null\vfill
    \vspace*{1cm}
    \noindent
    \begin{minipage}{0.35\linewidth}
        \begin{flushleft}
            \printauthor
        \end{flushleft}
    \end{minipage}
\end{titlepage}
\ClearShipoutPicture
\AddToShipoutPicture{\BackgroundPicNormal}

\tableofcontents
\chapter{System Design}

\section{Overview}

The normal VPN usage for bypassing filtering is to allow users with filtered internet access route their traffic through servers outside of their country. The VPN provider is usually a commercial entity that can provide high powered servers with good bandwidth, each serving many users.

One fundamental aspect of this approach is that users have to trust the VPN provider. If they doubt the authenticity of the VPN provider or its security, they may not feel safe in freely pursuing their normal activities. An example of this is observed in certain countries where the VPNs are sold in the ``black market''. There are rumors that the VPNs permitted on these markets are owned by the government itself, or have contracts with said government to share their information. As a result, users still feel their traffic is monitored even while using these VPNs.

Furthermore, there are accusations that for-profit VPN provider companies sell users' traffic information to third party corporations and advertisement networks. EFF has a guide \footnote{\href{https://www.eff.org/deeplinks/2017/04/heres-how-protect-your-privacy-your-internet-service-provider}{EFF articel}} regarding this issue. 

A recent FTC complaint \footnote{\href{https://cdt.org/insight/cdts-complaint-to-the-ftc-on-hotspot-shield-vpn/}{FCC complaint}} by Center for Democracy \& Technology about the VPN provider HotspotShield highlights such claims.

%%TODO: add the study on VPNs and data privacy

\subsection{Our Approach}
There are multiple ways to ``disrupt'' this environment. Our approach is to build the trust model on existing relationships in real world. If some trusted party is living in a free country, we aim to give them (automated)  tools that allow them to become the VPN provider for their colleagues, activist friends, and family members living inside the country with filtering.

Even if the VPN provider wishes to use their home VPN server for their own safety when travelling or on unsecured networks such as coffeeshops, they can benefit from our model without having to pay and trust an external entity.

Similar projects have been implemented before, but our emphasis is on providing an ``always on'' device specifically for this purpose.

\section{Usage and Terminology}
Our approach creates two classes of users for our tools:
 
\begin{itemize}
\item{\textbf{Person(s) behind filtering}:} 
We call this person simply our \gls{user}.
\item{\textbf{Person in a free country.}:}
We call this person the \gls{provider} since they \emph{provide} these services. We give each provider a pre-programmed physical device(below).
\end{itemize} 

This way, the users need to only ``trust'' their own provider, as that provider (and their ISP in the free country) is the only one with access to the traffic.

The layers of software used are shown in Figure \ref{UsageLayers}. We are in talks with other entities to provide a VPN client with embedded sTunnel support.

%http://texample.net/tikz/examples/pera-model/
%https://tex.stackexchange.com/questions/102563/how-to-draw-a-layered-architecture-using-latex
\usetikzlibrary{chains}

\vspace{20pt}

\begin{figure}
\pgfdeclarelayer{background}
\pgfsetlayers{background,main}
\definecolor{mygreen}{RGB}{202,217,126}
\begin{tikzpicture}[
	scale=0.75,
	start chain=1 going below, 
	start chain=2 going right,
	node distance=1mm,
	desc/.style={
		scale=0.75,
		on chain=2,
		rectangle,
		rounded corners,
		draw=black, 
		very thick,
		text centered,
		text width=8cm,
		minimum height=12mm,
		fill=blue!30
		},
	it/.style={
		fill=blue!10
	},
	ih/.style={
		fill=green!10
	},
	level/.style={
		scale=0.75,
		on chain=1,
		minimum height=12mm,
		text width=2cm,
		text centered
	},
	every node/.style={font=\sffamily}
]

% Levels
\node [level] (Level 4) {Application Level};
\node [level] (Level 3) {Proxy};
\node [level] (Level 2) {VPN Client};
\node [level] (Level 1) {sTunnel Client};
\node [level] (Level 0) {Internet};


% Descriptions
\chainin (Level 4); % Start right of Level 5

\node [desc, it] (UserApp) {Facebook/Twitter/Skype App};
\node [desc, it, right=of UserApp] (UserServer) {Facebook/Twitter/Skype};
\node [desc, ih, text width=8cm, xshift=2.25, below=of UserServer] (ZipRoxy) {Ziproxy Proxy};
\node [desc, ih, below=of ZipRoxy] (VPNS) {OpenVPN Server};
\node [desc, it, left=of VPNS] (VPNC) {OpenVPN Connect};

\node [desc, it, below=of VPNC] (sTunnel) {SSLDroid};
\node [desc, ih, right=of sTunnel] (sTunnelServer){sTunnel Server};
\node [desc, it, below=of sTunnelServer,text width=15cm, xshift=-4.4cm] (Internet) {Internet};

\begin{pgfonlayer}{background}
\draw[draw=black,fill=mygreen] 
  ([xshift=-1pt,yshift=1pt]VPNC.north west) rectangle 
  ([xshift=1pt,yshift=-1pt]sTunnel.south east);
\end{pgfonlayer}

  
\end{tikzpicture}
\caption{Usage layers \label{UsageLayers}}

\end{figure}


\section{Device Structure }

Hardware used as provider’s VPN server is a \gls{Raspberry Pi} 3:

\begin{itemize}
\item{SoC: Broadcom BCM2837}
\item{CPU: 4× ARM Cortex-A53, 1.2GHz}
\item{GPU: Broadcom VideoCore IV}
\item{RAM: 1GB LPDDR2 (900 MHz)}
\item{Networking: 10/100 Ethernet, 2.4GHz 802.11n wireless}
\item{Bluetooth: Bluetooth 4.1 Classic, Bluetooth Low Energy}
\item{Storage: microSD}
\item{GPIO: 40-pin header, populated}
\item{Ports: HDMI, 3.5mm analogue audio-video jack, 4× USB 2.0, Ethernet, Display Serial Interface (DSI)}

\end{itemize}

\subsection{Choice of VPN software}
Software package used for providing the VPN service is OpenVPN\footnote{\url{http://openvpn.net/}}.  sTunnel\footnote{\url{https://www.stunnel.org/}} is used to provide a tunnel disallowing Deep Packet Inspection of VPN packets. More governments are moving towards dropping packets if they are detected to be containing VPN data. The port will also be the either the usual HTTPs port (443) or similar, so port based filtering cannot be used by the government. Users are authenticated using a  certificate.

As of early 2018, we started switching our model to use ShadowSocks VPN tunnels. ShadowSocks allows masquarading traffic as normal web traffic, making it harder to filter it by automated tools. Furthermore, easy to use clients provided by Outline \footnote{\url{https://getoutline.com}} have a great user experience. The only issue we see in the general design is use of a symmetric password for traffic encryption. In OpenVPN + sTunnel use of assymetric public and private keys allowed verification of identities.

At the time this overview is updated, we have left OpenVPN+sTunnel code in place, but have disabled it. We will make judgment calls in the future either to switch back, or stick with Outline+ShadowSocks. We may also leave it to users, or provide a multi-layer classification tools to allow using both, each for a different type of users: more sensitive users can use OpenVPN + sTunnel, day-to-day users can use Outline + ShadowSocks.

\subsection{Location and Address}
The IP address of the VPN ``server'' is the home IP address of the provider. For example, it will be the address Comcast or AT\&T allocated to them. We only support connecting the device to the network using an Ethernet cable, as WiFi has shown to be hard to manage at times. 

We also inform providers they cannot put the device at locations such as their work place or public networks. Since they do not own such networks, it can cause serious legal problems for them and expose using an Ethernet cable, as WiFi has shown to be hard to manage at times. 

We also inform providers they cannot put the device at locations such as their work place or public networks. Since they do not own such networks, it can cause serious legal problems for them and expose traffic of users to the network owner.

\subsection{Outgoing Traffic, Command and Control}
A transparent proxy (Ziproxy\footnote{\url{http://ziproxy.sourceforge.net/}}) is used to cache HTTP pages, and in cases also save some bandwidth for users by reducing page or images sizes and similar. This is an optional flag. We experimented with this and got some very good improvements. It is also able to block advertisements which again not only save traffic, also improve privacy of users.

To send commands to the devices of all providers, Message Queue Telemetry Transport (\gls{MQTT}) \footnote{\url{http://mqtt.org/}} is used. These allow configuring the devices remotely when needed. Commands to add or delete users, changing certain features, or updates about IP address are carried over this channel. Beyond periodic updates, we can directly instruct devices to update our software or the other software packages it uses through this channel.

The Python web framework used for the server side is Django \footnote{\url{https://www.djangoproject.com/}}. It allows us to have a structured ``application'' with its own authentication and management. Actively maintained for the past 12 years, Django is very popular among web frameworks.

%We decided not to go through with this, given the feedback
%The device also provides a simple RSS feed reader. At our first version we will only support a default list of websites, as RSS feeds are not truly unified and each has a smaller variation requiring some work. The idea is that the users can also check their popular RSS feeds without the need for an app or software just by visiting the webserver on the device (and a provided username/password).

%Based on the user feedback, we might merge this with approaches like Khoondi\footnote{\url{https://asl19.org/en/khoondi.html} } app, or other popular RSS readers. This feature is a potential security risk as well, see below (\ref{RSS-Sec}).

\chapter{Essential Processes}
Each process includes multiple iterations of API calls, as described in the next chapter. Emphasis here is to provide an overview of the process, both as a software implementation guideline and a general overview.

\section{On-boarding}
\ref{OnBoardingSeqDiag}
\begin{figure}
  \centering

  \begin{sequencediagram}
    \newthread{p}{Provider}{Provider}
    \newthread{pa}{ProviderApp}{ProviderApp}
    \newinst{cs}{Central Server}{CentralServer}
    \newinst{rp}{Raspberry Pi}{RPi}
    %\newinst[1]{user}{User}{VPN User}

    \begin{call}{p}{plug device in}{rp}{}
    \end{call}
    \begin{call}{rp}{generate $device\_key$}{rp}{}
    \end{call}
   	\mess{rp}{show device key on LCD}{p}
    \begin{sdblock}{Claim}{Claim process}
      \begin{call}{p}{Install app}{pa}{}
      \end{call}
      \begin{call}{p}{Register as provider}{pa}{}
      \end{call}
      \begin{call}{p}{Enter $device\_key$ shown on display, and $serial$ number printed on device}{pa}{Result}
      	\begin{call}{pa}{Claim($serial,device\_key$)}{cs}{Result}
      	      	\begin{call}{cs}{if($serial$ is valid)}{cs}{set $device\_key$}
				\end{call}
      	\end{call}
      \end{call}
    \end{sdblock}
  \end{sequencediagram}
    \caption{On-Boarding process}
    \label{OnBoardingSeqDiag}
\end{figure}


\section{Troubleshooting}

Addition of LCD screens to devices allowed us to provide status and error message on the device. For example, if the device is not connected to internet it can show an icon indicating that. If user needs a more comprehensive message, they can initiate a self-diagnostic process by pressing ``Diagnostics'' button on the device. This will run several tasks (check ethernet IP, external IP, ability to ping our server, etc.) and show corresponding error messages as well as solutions.

If the self-diagnostics method does not resolve the issue, provider can contact our customer support number and arrange a live debugging session if needed. While this process can be helpful to many, we anticipate it to be targeted for intrusion and accessing user information.

One of our security goals is to prevent easy social engineering methods when contacting customer support and impersonating a provider person. To do so, we added one more requirement when making customer service requests: a random pin number.

The Raspberry Pi device periodically regenerates a pin. This pin is to be displayed on the device LCD, and also sent to central server as part of HeartBeat monitor messages (\ref{HBSeqDiag}). If the user claims to have access to the device, they will be asked to read this pin to prove that.



\section{Adding and removing a User}
\begin{figure}[H]
  \centering

  \begin{sequencediagram}
    \newthread{pa}{ProviderApp}{ProviderApp}
    \newinst{cs}{Central Server}{CentralServer}
    \newinst{rp}{Raspberry Pi}{RPi}
    \newinst[1]{user}{User}{VPN User}

    \begin{sdblock}{AddUser}{}
    \begin{call}{pa}{AddUser(email, cert\_name)}{cs}{}
     	\begin{call}{cs}{AddUser(email,cert\_name)}{rp}{}
      	\end{call}
     	\mess{rp}{send cert via email}{user}
    \end{call}
   \end{sdblock}

    \begin{sdblock}{DelUser}{}
    \begin{call}{pa}{DelUser(email}{cs}{}
     	\begin{call}{cs}{DelUser(email,cert\_name)}{rp}{}
      	\end{call}
     	\begin{call}{rp}{revokeCert(cert\_name)}{rp}{}
      	\end{call}
     	\begin{call}{rp}{reloadVPN()}{rp}{}
      	\end{call}
     	\mess{rp}{send notice via email}{user}
    \end{call}
   \end{sdblock}


  \end{sequencediagram}
    \caption{Add/delete user process}
    \label{AddUserSeqDiag}
\end{figure}


\section{Heartbeat and IP Address Changes}
\begin{figure}
  \centering

  \begin{sequencediagram}
    \newinst{cs}{Central Server}{CentralServer}
    \newinst{rp}{Raspberry Pi}{RPi}
    \newinst[1]{user}{User}{VPN User}

    \mess{rp}{HeartBeat}{cs}
    \begin{call}{cs}{Update(id,time,pin1,ip1,diag code, sw version, serial,status, port, device key)}{cs}{}
    \end{call}
    \mess{rp}{HeartBeat}{cs}
    \begin{call}{cs}{Update(id,time,pin2,ip1, ...)}{cs}{}
    \end{call}
    
    \begin{sdblock}{IP has changed}{}
      \mess{rp}{HeartBeat}{cs}
      \begin{call}{cs}{Update(id,time,pin3,ip2, ...)}{cs}{}
      \end{call}
      \begin{call}{cs}{if $ip_1 != ip_2$, send another add\_user command to RPi}{rp}{}
	      \mess{rp}{send updated cert via email with new IP}{user}
      \end{call}

    \end{sdblock}
  \end{sequencediagram}
    \caption{Heartbeat messages and handling updated IP}
    \label{HBSeqDiag}
\end{figure}

When an $add\_user$ command arrives at a RPi device, it will contain a certificate name. For truly new users, it will be randomly generated. For existing users who need to be notified about a new address, it will be the same as what they had before. RPi process will first check to see if a certificate with the same name already exists. If so, only the IP change will be announced in the outgoing email.

\chapter{Security Concerns}
%TODO add the threat scenario document here
\section{Overview}

Fortunately, the P2P nature and topology of WE-PN automatically mitigates a number of issues classic VPN providers face in the country of interest. Those issues include large volume of encrypted traffic going towards certain IPs (as they use servers in data centers) which makes flagging and blocking of those servers much easier, but with P2P services like WE-PN these amounts of traffic is negligible. This will also help in operating in a lower threat level, because when the adversary does not have a large target, it does not make any sense to run sophisticated operations (throttling, MITM, etc.) for very small amounts of traffic.

However, the above does not mean our network will not be subject to various attacks. In fact, in cases it might mean the attacks are more tageted depending on if the adversaries can identify an end user whose traffic is of high value. We also anticipate the ususal random attacks, using scripts scanning for vulnerabilities. 

Security is probably the single most important element in such a project. We have divided the concerns into 4 main categories in the Table \ref{TblSec}.

\begin{table}
	\centering
    \begin{tabular}{ccccc}
	\toprule
	Physical & Storage & Internet\/Transit & Endpoint \\ \toprule
	Physical access & \gls{RPi} server  & DNS Resolvers & Malware \\\cmidrule{3-4}
	to RPi & storage encryption   & Traffic isolation & Illegal activities \\\midrule
	& & Firewall & \\\bottomrule
    \end{tabular} 
        \caption{ Security Categories	\label{TblSec}}
\end{table}
    
\section{Physical Security}

As providers and probably people around them will have physical access to these devices, it is very important to communicate the significance of physical access to them. They should know not to put these devices in a public location (offices, etc.) as they can be misused by somebody with enough technical abilities.

\subsection{Claiming a device}
One aspect of on-boarding discussed is the process of ``claiming'' a device. Provider, upon receiving the hardware, will use the on-boarding process to tell the server that the device now belongs to them. Many IoT devices use simple printed PINs attached to the device to allow user to claim the device.

We initally followed this design as well. But after considering the chance of people trying to transfer the device due to lack of use (as described in Section \ref{homeNetAccess}), we decided to add an LCD and show a dynamic device key. The dynamic nature of the device key would allow a new provider to prove they have the device, and subsequent randomly generated PINs will show they still own the device. Same LCD can show error messages and current state.

We are afraid this process will hinder the user experience for providers. Since we have a small pool of users at the start, we are adding the beta testers directly. We will assign them the device once they request and are approved for the device. However, the PIN structure is present in all software sections (except the PIN display).

\section{Storage Security}
%TODO this seems like copy-paste, update about what we did

Encryption: It would be a great protection layer, if Raspberry PIs could be shipped with LUKS storage encryption enabled\footnote{An elaborate manual for it can be found here: \url{https://robpol86.com/raspberry_pi_luks.html}}. There are some technical difficulties (for auto mounting a LUKS encrypted partition during boot with a key file) but it’s possible. If total encryption could be problematic, encrypting home directory and storing sensitive data like certificates there could be an option.

It’s been proven that storage encryption has very little effect on the storage performance even on the A and B models, therefore this would not be an issue at all on the model 3 with much better CPU performance. The storage encryption will makes it impossible for somebody to put the memory on another device and try to read or possibly modify the content.

We explored the options, and it seemed like encrypting the storage would make it one step harder for an attacker but not too hard. Our headless installations mean the encryption has to be opened with a key, stored on the same device. An attacker can clone the SD card in minutes and decrupt it later.

%TODO remove, as we do have comments
We appreciate any comments from security auditors on this topic.

\section{Internet / Transit}
\subsection{Django Framework}

One major advantage of using Django framwork is its comprehensive security features built in. Details of its security mitigation methods are available here: \url{https://docs.djangoproject.com/en/1.11/topics/security/}.

We have obviously also used Let's Encrypt to encrypt web traffic and MQTT communications with clients.

\subsection{E-Mail for VPN certificate delivery}
Users in the country with filtered (or suspected to be compromised) internet are supposed to receive the VPN `certificate' file. A well known security advice to any user is ``do not open attachments''. We tried our best to avoid using email, but right now it seems like the best option.

First, it is easily available and users with any level are familiar with it. Second, it is very hard to filter, and with proper email server setup it is also hard to compromise. Should we have used a method to deliver from a certain ``certificate server'', that server would be quickly filtered. As a result, we have used email as our `last mile' delivery method, unless Security Auditors find a compelling reason not to.

Another challenge remaining is phishing. Should a third party forge an email identical to ours to a target user with a VPN certificate, end user cannot easily identify the forgery. As a result, an attacker can divert traffic to their VPN server. To avoid that, we added a `shared secret' or 'passphrase' to our application. While setting up a new VPN user, provider and user agree on a phrase to authenticate the emails received. All certificate emails contain this phrase.

This relies on the user to make sure the emails do contain the right security phrase. We will include information on this in our educational material.

%TODO add the text I added on scratch pad, and link Cisco. Also mention PhishTest approach for education

\subsection{DNS Resolvers} 

It is strongly recommended not to use default ISP DNS servers and use reputable, public DNS servers instead. Although they may provide slightly better performance as they have a lower latency to the operator, it’s generally safer to use recognized resolvers.

Traffic isolation in servers: Although OpenVPN isolates traffic between each VPN user by default, it’s important to make sure in the settings and check with a packet analyzer.

\subsection{Firewall}
%TODO traffic is filtered to local IP addresses
The Raspberry Pis use iptables as their firewall and for forwarding traffic. We have not finalized the set, and can benefit from recommendations from security auditors. These devices will be used mostly behind NAT, but we prefer not to rely on the home router to protect the devices.

The server firewall is also the AWS firewall. All ports except the ones necessary are filtered.

\section{End Point Security}

\subsection{Malware}
An infected user can expose provider's network to malware as well. While we use firewalls to limit access of the VPN users to the host network, it is expected that user are also made aware of the need for digital hygine.


\subsection{Illegal activities}
 While certain activities in some countries may not exactly be considered illegal (or fall in a gray area), they can cause difficulties in some other legal jurisdictions. For instance, downloading files by torrent protocol in one country of interest may cause no issues. But if their operator is based in Germany for instance, downloading copyrighted materials via Torrent could quickly become a legal issue for the operator. 

Providers should be advised in manuals and be asked to transfer such advice to their own respective users.


\subsection{Logging}
We still have some logging turned on for debugging, we are removing as many prints as possible as our software matures. We want to make sure that the traces left on the device and server are as who is added for VPN access.

\subsection{RSS Reader: unintended consequences}
\label{RSS-Sec}

We initially planned to also include an RSS reader on the devices. The idea was that the nginx on the device would forward RSS requests to our RSS reader web application. The RSS reader, a simple interface, could be accessed only using the URL.

However, we soon realized that users (and operators) were very interested in publicly releasing the URL. Making such move would quickly compromise a critical feature of our work: being only known to small group of users. We do \textbf{not} rely on security by obscurity, but we do not want the filtering authorities to quickly block IP of devices or monitor who accesses them.

As a result, we have shelved our RSS reader subplan for now.


\section{Access control}

\subsection{Private channel per device}
Obviously list of users of each device has to be only accessible to the target device. As a result, each device is provided with its own private MQTT ``topic''. The server loops through devices assigned to an operator when a new command is issued, and informs all of them. Our design gives access to all devices the same operator has to his/her ``\gls{friend}s''.

\subsection{Prevent device users to see home network of provider}
\label{homeNetAccess}
Users should not be able to see traffic inside the home, for example should not be able to cast to TV or sniff traffic of provider. Our iptables should be able to do that in conjunction with the tunnel created for VPN traffic.

\subsection{Access to list of other VPNs, emails of other users}
Our biggest remaining concern is that emails of users should not be known to anyone. We already restrict access to such lists on the Django side based on authenticated provider user. Our main remaining concern is that the emails are stored on the database. In case the server is compromised, we are worried about access to this list.

However, the current implementation practices we researched also follow similar designs. Any better approached recommended from security audtors are most appreciated.


\section{Long-term maintenance}
\label{longterm}
IoT devices undergo much neglect after the initial installation. We have setup an \gls{APT} repository \footnote{\url{https://repo.we-pn.com}} and provide our software to the endpoint RPi devices using APT. The devices report their software version as part of the heartbeat messages, so we can monitor if any devices are out of date.


\section{Ownership transfer}
Another aspect of long-term maintenance is dealing with the fact that some providers may not like our work. What if one provider gives their device to another person? Part of our project is depending on the trust between VPN provider and users on a personal level: they trust provider would notify them and take proper action.

Proper action in here is to wipe access configurations on the device, and new provider adding users again (should they trust each other).


\section{Educating the Provider}
Pending some paperwork, we plan on making multiple educational material for users and providers. Providers should know that their IP is used in any activity of users, so they have to trust only people they directly know. 

Another convern is how a comproised user will impact the network. What if someone gets arrested? Our work in Section \ref{homeNetAccess} should block access to home network of provider, but still does not prohibit doing other indirect attacks. An example would be using the IP of provider to leave a trace for malicious activies on purpose. Once again, we can only help the provider by making it east to remove users quickly and immediately. Anything beyond that goes into educating the provider.

\printglossary

\end{document}
\grid
