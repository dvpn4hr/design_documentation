Last updated: June 2023

https://we-pn.com/index.html

# Privacy Policy - WEPN

## What we do not collect

We do not have access to either internet traffic of WEPN providers (also knowns as WEPN pod owners) or the traffic of their users (also called friends). Also we cannot remotely connect (SSH, VNC, etc.) to your WEPN device, unless you explicitely enable such feature on your device and give us access.

## What we do collect

As of June 2023, we store the **email addresses** of WEPN providers and friends to whom they give access to, the IP addresses of WEPN devices, as well as device diagnostic/status information.

To allow your friend to know emails are coming from your device and not an imposter, we allow you to set a **"familiar phrase"** for each of your friends. You can pick a random phrase (such as "red apple") and inform them beforehand of this phrase using another messaging service (such as WhatsApp message, etc.). This phrase is __saved in plain text on our servers__, and is sent (__again in plain text__) in each email that goes to them from your device.

If you submit a support ticket, **the information you submit** will be also recorded and attached to the ticket with Zendesk. If you choose to also send **error logs** from your device, the logs will also be recorded. We have taken steps to remove Personally Identifiable Information (PII) from these error logs as much as possible. You can also review and edit these logs before being sent, directly in the app.

### Device Health Data

Your device and our server periodically run end to end tests that show if the services are running. The information about the test and its result are also kept on the server.
As of June 2023, this list includes:

* **serial_number**: serial number of the device, assigned in factory
* **ip_address**: external ip address of the device
* **status**: integer showing if device is up, down, restarting, available, ...
* **pin**: randomly generated token, updated periodically
* **local_token**: randomly generated token, updated periodically
* **local_ip_address**: IP address on the local network
* **device_key**:a key that the device uses to authenticate itself to the server; it is randomly generated at time of claiming it
* **port**: the main port number used by the software,
* **software_version**: version of our software,
* **diag_code**: an integer showing if the device has any issues (connectivity, port forwarding, etc.)
* **access_cred**: a one-way hash of credentials of each user (not decryptable by WEPN staff), which allows the mobile app to know if it needs to sync with the device again,
* **usage_status**: a list of if each user has used their services recently or not
* **public_key**: device generated keys for secure communication with server
 
We try to update this list as it changes over time, but you can access this via the source code here: https://source.we-pn.com/home_device/src/master/usr/local/pproxy/heartbeat.py

### Manual Compatibility Tests on Provider's Phone App

Before setting up a device, and as part of debuggin attempts, we may ask you to run the Compatibility Test on our mobile app. This will examine your network, and upload the following information to our server:

* Public IP address of the device,
* Router information: admin URL, main URL, port, friendly name, manufacturer, manufacturer website, model information, model name
* Log of error encountered during the test
* Measured network speed
* filtered list (port, description) of existing network port forwarding rules only if their descriptions start with words "wepn" or "shadowsocks".

Through extended testing, we have seen the need for this information for debugging. We try to keep this information minimal, and will do our best to update the list if we add new items. The code sending this information can be reviewed here: https://source.we-pn.com/mobile_app/src/master/src/hooks/useCompatibilityProcess.js


We shall comply with any applicable legal procedures that request the above information.

## Third party information collected

As noted above, when you submit a support ticket, the information you submit will be also recorded and attached to the ticket with Zendesk. Their privacy policy is available here: https://www.zendesk.com/company/agreements-and-terms/privacy-policy/

Our mobile apps are distributed via Google and Apple services, and thus may subject you to their privacy policies: https://policies.google.com/privacy and https://www.apple.com/legal/privacy

The open source products installed as part of the process may have their own privacy policy, and we encourage you to read them.

## How we use this information

The information collected will allow our software to function, and some of it are shown to Providers and Users as the design of our products and services mandates. We also use this information to debug issues, provide support, understand our performance and issues, and plan new features and upgrades.


## How we will not use this information

We do not sell this information or the “metadata” associated with you to third parties for advertising or marketing purposes. We also do not provide API access for marketing purposes to third parties.

We also do not use this information for marketing purposes ourselves, or show you ads in our app or website, or through your home device. We do not inject any advertisements or tracking cookies into end users' traffic.

## How you can request a copy or erasure of this information

Please reach out to us via support at we-pn . com with the subject line “Request: my Information”. Please note that we will authenticate your identity, for example via the email address you provided during setup, to process your request.

## Changes to this privacy policy

We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately after they are posted on this page.

## Children’s privacy

These Services do not address anyone under the age of 13. We do not knowingly collect personally identifiable information from children under 13. In the case we discover that a child under 13 has provided us with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions.


## Security

We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it available to us. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.
